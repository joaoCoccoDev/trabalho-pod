def resursiveInsertionSort( array:list , n:int ):
    if n <= 1: 
        return;
    resursiveInsertionSort( array , n-1)
    nth = array[n-1]    
    j = n-2;
    while j >= 0 and array[j]> nth:
       
        array[j+1] = array[j]
        j = j - 1 
        
    array[j+1] = nth

v = [ 12,90 , 7, 51, 34]
print('vetor inicial')
print(v)
resursiveInsertionSort(v, v.__len__())
